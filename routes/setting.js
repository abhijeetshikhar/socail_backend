const express = require("express");
const router = express.Router();
const multer = require('multer')
const {profileImages} = require('../controllers/setting')
const  auth = require('../middleware/auth')

let storage = multer.diskStorage({
	destination: function (req, file, callback) {
		console.log("file", file);
		callback(null, "./Uploads/");
	},
	filename: function (req, file, callback) {
		// console.log("multer file:", file);
		const fileName = file.originalname.toLowerCase().split(' ').join('-');
		
		callback(null, fileName);
	}
});

let maxSize = 1000000 * 1000;
let upload = multer({
	storage: storage,
	limits: {
		fileSize: maxSize
	},

});



router.post('/profileimages', auth, profileImages)

router.post('/multiple', upload.array('avatar',4), profileImages)


module.exports = router;
