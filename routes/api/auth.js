const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const auth = require('../../middleware/auth');
const jwt = require('jsonwebtoken');
const User = require('../../models/User');
const crypto = require('crypto')
const { emailFunc } = require('../../config/emilTemp');

// @route    GET api/auth
// @desc     Get user by token
// @access   Private
router.get('/', auth, async (req, res) => {
  try {
    const user = await User.findById(req.user.id).select({ password: 0, resetToken: 0, expireToken: 0 });
    res.json(user);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

// @route    POST api/auth
// @desc     Authenticate user & get token
// @access   Public
router.post('/', async (req, res) => {
  const { email, password } = req.body;
  if (!email || !password) {
    return res.status(400).json({
      error: "Please select all fields"
    })
  }
  try {
    let user = await User.findOne({ email });
    if (!user) {
      return res.status(400)
        .json({ errors: "user not exists" });
    }
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      return res
        .status(400)
        .json({ errors: "wrong pass" });
    }

    const payload = {
      user: {
        id: user.id,
        email: user.email,
        name: user.name,
        role: user.role

      }
    };

    jwt.sign(
      payload,
      process.env.JWT_SECRET,
      { expiresIn: 360000 },
      (err, token) => {
        if (err) throw err;
        res.json({ token });
      }
    );
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server error');
  }
}
);





// Reset Password Route
// Post Routes
// Routes ==>> http://localhost:8080/api/auth/reset-password
// Payload: email          

router.post('/reset-password', async (req, res) => {

  crypto.randomBytes(32, async (err, buffer) => {
    if (err) {
      console.log(err)
    }
    const token = buffer.toString("hex")
    try {
      let user = await User.findOne({ email: req.body.email })
      if (!user) {
        return res.status(400).json({
          error: "User not found"
        })
      }
      user.resetToken = token
      user.expireToken = Date.now() + 600000
      await user.save()
      await emailFunc(user, token, (cb) => {
        console.log("Hi i am ", cb)
        if (cb.status == 200) {
          return res.json({ message: "Email send" })
        }
        else if (cb.status === 400) {
          res.message({ message: cb.error })
        }
      })
    } catch (error) {
      console.log(error)
      return res.status(500).send("Server Error")
    }
  })
})

// New Password Route
// Post Routes
// Routes ==>>http://localhost:8080/api/auth/new-password
// Payload: password,
//          token         


router.post('/new-password', (req, res) => {
  const newPassword = req.body.password
  console.log("------------------------------------", newPassword)
  const sentToken = req.body.token
  User.findOne({ resetToken: sentToken, expireToken: { $gt: Date.now() } })
    .then(user => {
      if (!user) {
        return res.status(400).json({ error: "Try again session expired" })
      }
      bcrypt.hash(newPassword, 12).then(hashedpassword => {
        user.password = hashedpassword
        user.resetToken = undefined
        user.expireToken = undefined
        user.save().then((saveduser) => {
          res.json({ message: "password updated success" })
        })
      })
    }).catch(err => {
      console.log(err)
    })
})


// Change Password Route
// Post Routes
// Routes ==>> http://localhost:8080/api/auth/changePassword
// Payload: currentPassword
//           newPassword


router.post("/changePassword", auth, async (req, res, next) => {
  let { password, npassword } = req.body;
  console.log(req.body)
  try {
    //Check if user already exists
    let user = await User.findOne({ _id: req.user.id });
    if (!user) {
      return res.status(400).json({ errors: [{ msg: 'You are omitting the privacy policies' }] })
    }
    //compare passwords
    const isMatch = await bcrypt.compare(password, user.password)
    if (!isMatch) {
      return res.status(400).json({
        error:"Old password wrong"
      })
    }
    //Encrypt the password using bcryptjs
    const salt = await bcrypt.genSalt(10);
    let newPassword = await bcrypt.hash(npassword, salt);
   
    await User.findOneAndUpdate({ _id: req.user.id }, { $set: { password: newPassword } }, { new: true });
    if (npassword === password) {
      return res.status(400).json({
        error: "old and current passwrod not same"
      })
    }
    res.status(200).json({ message: 'password updated succesfully'  })
  } catch (error) {
    console.log(error)
  }
})



module.exports = router;
