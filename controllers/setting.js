const User = require('../models/User')
const AWS = require('aws-sdk')
const fs = require('fs');
const { user } = require('../routes/setting');
const Busboy = require('busboy');
const tempURL = "./temp"
//s3 configure
const s3 = new AWS.S3({
	accessKeyId: process.env.AWS_ACCESS_KEY_ID,
	secretAccessKey: process.env.API_SECRET_ACCESS_KEY,
	region: process.env.AWS_REGION
});


exports.profileImages = async (req,res)=>{
	var busboy = new Busboy({ headers: req.headers });
	var flagFile = false
	var fileData;
	var val;
	busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
		flagFile = true
		//Creating a writable stream for saving the data in file..
		fileData = Date.now() + "_" + filename;
		var fileRead = fs.createWriteStream(tempURL + fileData);
		file.on('data', function (data) {
		});
		file.pipe(fileRead)
		/**Read from uploading file read stream and write into writable stream of local file*/
		file.on('end', function () {
			console.log("end")
		});
	});
	busboy.on('field', async function (fieldname, value, fieldnameTruncated, valTruncated, encoding, mimetype) {
		console.log("Line no 34", value)
		if (value) {
			try {
				console.log(value)
				val = JSON.parse(value)
			} catch (err) {
				console.log(err)
			}
		}
	});
	busboy.on('finish', async function (data) {
		console.log("Data", data)
		//UPDATE AWS CONFIG...
		//UPDATE AWS CONFIG...
		AWS.config.update({
			accessKeyId: process.env.AWS_ACCESS_KEY_ID,
			secretAccessKey: process.env.API_SECRET_ACCESS_KEY,
			region: process.env.AWS_REGION
		});
		try {
			let email = req.user.email
			let user = await User.findOne({ email })
			if (user) {
				if (flagFile) {
					var params = {
						Body: fs.readFileSync(tempURL + fileData),
						Key: `product/${fileData}`,
						ACL: 'public-read',
						Bucket: 'abhijeetstore'
					};
					let data = await s3.upload(params).promise()
					saveData(req, res, val, data, fileData)
					//Now check the all fields first
				} else {
					saveData(req, res, val, data, fileData)
				}
			}
		} catch (error) {
			console.log(error)
			res.status(500).json({ msg: 'Internal server error' })
		}
	});
	req.pipe(busboy);
}



async function saveData(req, res, val, data, fileData) {
	console.log(data)
		if (val.email !== '' || val.email) {
			if (val.name !== '' || val.name) {
				//Now check the User in DB
				let user = await User.findOne({ email: val.email })
				console.log(user)
				if (user) {
					user.email = val.email;
					user.name = val.name;
					if (data) {
						user.avatar = data.Location
					}
					//lets save the user
					await user.save();
		
					if (fileData) {
						fs.unlink(tempURL + fileData, (err) => {
							if (err) {
								console.log(err)
							}
						})
					}
					res.status(200).send({
						name:user.name,
						email: user.email,
						avatar:user.avatar

					})
				}
			} else {
				res.status(400).send({ message: 'Invalid email' })
			}
		} else {
			res.status(400).send({ message: 'Invalid name' })
		}
}

exports.multipleImage = (req,res)=>{
	
}