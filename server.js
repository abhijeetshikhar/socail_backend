const express = require('express');
const connectDB = require('./config/db');
const cors = require('cors')
const app = express();
const logger = require("morgan");
require("dotenv").config();
const setting = require('./routes/setting')
const auth= require("./routes/api/auth");
// Connect Database
connectDB();

var origin = '*'

app.use((req, res, next) => {
  console.log("head", req.headers);
  if (req.headers.origin) {
    origin = req.headers.origin
  } else {
    origin = "*"
  }
  next();
})
app.use(cors({ credentials: true, origin: origin }));




app.get('/', function (req, res) {
  res.send("Hello, welcome Abhijeet! How r u ")
});

app.use(logger('combined'));
// Init Middleware
app.use(express.json({ extended: false }));

// Define Routes
app.use('/api/users', require('./routes/api/users'));
app.use('/api/auth', require('./routes/api/auth'));
app.use('/api/profile', require('./routes/api/profile'));
app.use('/api/posts', require('./routes/api/posts'));
app.use('/api',setting)



const port = process.env.PORT || 8080;

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
