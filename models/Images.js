const mongoose = require('mongoose');
const UserSchema = new mongoose.Schema({

	avatar: [],
	date: {
		type: Date,
		default: Date.now
	}


});

module.exports = User = mongoose.model('user', UserSchema);
