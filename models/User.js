const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    //required: true,
    unique: true,
    lowercase:true
  },
  password: {
    type: String,
    //required: true
  },
  token: {
    type: String,
    required: false
  },
  role: {
    type: String,
    default: "subscriber"
  },
  avatar: {
    type: String,
    default: "https://s3.eu-central-1.amazonaws.com/bootstrapbaymisc/blog/24_days_bootstrap/fox.jpg"
  },
  resetToken: String,
  expireToken: Date,
  date: {
    type: Date,
    default: Date.now
  }

  
});

module.exports = User = mongoose.model('user', UserSchema);
