const jwt = require('jsonwebtoken');
const config = require('config');
const User = require('../models/User');
const { use } = require('../routes/setting');

module.exports = async function(req, res, next) {
  // Get token from header
  const token = req.header('jwt');

  // Check if not token
  if (!token) {
    return res.status(401).json({ msg: 'No token, authorization denied' });
  }

  // Verify token
  try {
    await jwt.verify(token, process.env.JWT_SECRET, (error, decoded)=>{
      if(error){
        res.status(401).json({ msg: 'Token is not valid' });
      }
      else{
        req.user = decoded.user;
        console.log("----hiiiiii---",req.user)
        next();
      }
    });
  } catch (err) {
    console.error('something wrong with auth middleware')
    res.status(500).json({ msg: 'Server Error' });
  }
};


exports.userMiddleware = (req, res, next) => {
  try{
    let user = User.findById({id:req.user.id})
    if(!user){
      return res.status(400).json({
        error:'User not found'
      })
    }
    if (user.role !== 'subscriber'){
      return res.status(400).json({
        error:'Access denied'
      })
    }
    req.profile = user;
    next()
  }catch(err){
    console.log(err)
  }
};

exports.addminMiddleware = (req, res, next) => {
  try {
    let user = User.findById({ id: req.user.id })
    if (!user) {
      return res.status(400).json({
        error: 'User not found'
      })
    }
    if (user.role !== 'subscriber') {
      return res.status(400).json({
        error: 'Access denied'
      })
    }
    req.profile = user;
    next()
  } catch (err) {
    console.log(err)
  }
};