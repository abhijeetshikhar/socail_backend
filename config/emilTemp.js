
const nodeMailer = require('nodemailer');
const emailFunc = (user, token, cb) => {
    const transport = nodeMailer.createTransport({
        host: "smtp.gmail.com",
        port: 465,
        secure: true,
        auth: {
            user: process.env.user,
            pass: process.env.pass
        }
    });

    const mailOptions = {
        from: process.env.EMAIL_FROM,
        to: user.email,
        subject: `Password Reset link`,
        html: `
                <h1>Please use the following link to reset your password</h1>
                   <p>${process.env.CLIENT_URL}/reset/${token}</p>
                <hr />`
    }
    transport.sendMail(mailOptions, (error, res) => {
        if (error) {
            console.log(error);
            return cb({ status: 400 })
        }
        else {
            return cb({ status: 200 })
        }
    });


}
module.exports = { emailFunc }